class Staff < ActiveRecord::Base
  belongs_to :event
  belongs_to :user
  
  validates :name, presence: true, on: :create
  validates :age, presence: true, on: :create
  validates :phone_number, presence: true, on: :create
  validates :email_address, presence: true, on: :create
end
