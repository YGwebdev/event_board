class Event < ActiveRecord::Base
    belongs_to :user
    has_many :staffs
    
    validates :title, presence: true
    validates :position, presence: true
    validates :date_of_event, presence: true
    validates :last_date_of_event, presence: true
    validates :city, presence: true
    validates :location, presence: true
    validates :description, presence: true
    validates :pay, presence: true
end
