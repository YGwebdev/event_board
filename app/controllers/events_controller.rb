class EventsController < ApplicationController
    before_action :find_event, only: [:show, :edit, :update, :destroy]
    before_action :authenticate_user!, except: [:index, :show]
    
    def index
        @events = Event.all.order("created_at DESC")
    end
    
    def show
        @staff = Staff.new
    end
    
    def new
        @event = current_user.events.build
    end
    
    def create
        @event = current_user.events.build(event_params)
        
        if @event.save
            current_user.update_attribute(:has_event, 'true')
            redirect_to root_path 
        else
            render 'new' 
        end
    end
    
    def edit
    end
    
    def update
        if @event.update(event_params)
            redirect_to event_path(@event)
        else
            render 'edit'
        end
    end
    
    def destroy
        @event.destroy
        redirect_to root_path
    end
    
    private
        def event_params
            params.require(:event).permit(:title, :position, :date_of_event, 
                                          :last_date_of_event, :location, :pay, 
                                          :city, :description)
        end
        
        def find_event
            @event = Event.find(params[:id])
        end
end
