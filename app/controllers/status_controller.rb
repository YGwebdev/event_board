class StatusController < ApplicationController
    before_action :authenticate_user!
    
    def index
        @staffs = Staff.all
    end
    
    def show
        @event = Event.find(params[:id])
        @staff = @event.staffs
    end
    
    def destroy
       @staff = Staff.find(params[:id])
       @event = @staff.event
       @staff.destroy
       redirect_to status_path(@event)
    end
end
