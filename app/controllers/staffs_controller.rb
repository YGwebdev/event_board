class StaffsController < ApplicationController
    before_action :authenticate_user!
    
    def new
        @event = Event.find(params[:event_id])
        @staff = @event.staffs.build
    end
    
    def index
        @staffs = Staff.all
    end
    
    def create
        @event = Event.find(params[:event_id])
        @staff = @event.staffs.create(staff_params)
        @staff.user_id = current_user.id
        
        if @staff.save 
            redirect_to root_path
        else
            render 'events/show'
        end
    end

    private
        def staff_params
            params.require(:staff).permit(:name, :age, :phone_number, :email_address)
        end
end
