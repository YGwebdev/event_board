class AddPositionToEvents < ActiveRecord::Migration
  def change
    add_column :events, :position, :string
  end
end
