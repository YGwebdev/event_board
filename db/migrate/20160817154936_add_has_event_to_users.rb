class AddHasEventToUsers < ActiveRecord::Migration
  def change
    add_column :users, :has_event, :boolean, null: false, default: false
  end
end
