class AddEmailAddressToStaffs < ActiveRecord::Migration
  def change
    add_column :staffs, :email_address, :string
  end
end
