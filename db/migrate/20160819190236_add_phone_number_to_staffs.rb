class AddPhoneNumberToStaffs < ActiveRecord::Migration
  def change
    add_column :staffs, :phone_number, :integer
  end
end
