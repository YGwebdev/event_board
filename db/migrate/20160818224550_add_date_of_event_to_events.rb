class AddDateOfEventToEvents < ActiveRecord::Migration
  def change
    add_column :events, :date_of_event, :date
  end
end
