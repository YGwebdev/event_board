class AddLastDateOfEventToEvents < ActiveRecord::Migration
  def change
    add_column :events, :last_date_of_event, :date
  end
end
